/*
********************************************************************************
*                     IMPORTAR MÓDULOS NECESARIOS
********************************************************************************
*/

var util = require('util')
var memwatch = require('memwatch');
var fs = require('fs');

/*
********************************************************************************
*        MODIFICACIONES AL PROTOTIPO DE ARRAY (AGREGAR FUNCIONALIDAD)
********************************************************************************
*/

Array.prototype.diff = function(a) {
    return this.filter(function(i) {return !(a.indexOf(i) > -1);});
};

Array.prototype.unique = function(x) {
    var a = this.concat(x);
    for(var i = 0; i < a.length; ++i) {
        for(var j = i + 1; j < a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};

Array.prototype.minNat = function() {
  var minimum = -2;
  for(var i = 0; i < this.length; i++){
    if(minimum === -2 && this[i] > 0){
      minimum = this[i];
    } else if (this[i] < 0){
      continue;
    }
    
    if (this[i] < minimum){
      minimum = this[i];
    }
    
  }
  return minimum;
}

Array.prototype.count = function(x){
  var count = 0;
  for (var i = 0; i < this.length; i++){
    if(this[i] === x){
      count++;
    }
  }
  
  return count;
}

/*
********************************************************************************
*                          DECLARACIÓN DE CONSTANTES A*
********************************************************************************
*/

var CELDAS_DISP = 9;
var LADO_CUAD = 3;
var FACTOR_RIESGO = 10;
var INFINITO = 65536;
var TOTAL_CELDAS = 81;

/*
********************************************************************************
*                  DECLARACIÓN DE VARIABLES GLOBALES GENERALES
********************************************************************************
*/

var puzzle;
var error = false;
var snaps = [];
var ultimoVar = 0;

/*
********************************************************************************
*                      DECLARACIÓN DE VARIABLES GLOBALES A*
********************************************************************************
*/

var heuristicas;

/*
********************************************************************************
*        DECLARACIÓN DE VARIABLES GLOBALES SATSIFACCIÓN DE RESTRICCIONES
********************************************************************************
*/

var restricciones;

/*
********************************************************************************
*            DECLARACIÓN DE VARIABLES GLOBALES ALGORITMOS GENÉTICOS
********************************************************************************
*/

var setValues;
var geneticPuzzle;

/*
********************************************************************************
*            DECLARACIÓN DE VARIABLES GLOBALES ALGORITMOS GENÉTICOS
********************************************************************************
*/

var posibles;

/*
********************************************************************************
*       MÉTODOS DE OBTENCION DE DATOS (FILAS, COLUMNAS, CUADROS DE 3*3)
********************************************************************************
*/

function getFila(n){
  var result = [];
  var fila = Math.floor(n/CELDAS_DISP) * CELDAS_DISP;
  for (var i = 0 ; i < CELDAS_DISP ; i++){
    result.push(puzzle[fila + i]);
  }
  return result;
}

function getColumna(n){
  var result = []
  var columna = n % CELDAS_DISP
  for (var i = 0; i < CELDAS_DISP; i++){
    result.push(puzzle[columna + (i * CELDAS_DISP)]);
  }
  return result;
}

function getCuadro(n){
  var result = [];
  var cuadro = n - ((n % CELDAS_DISP) % LADO_CUAD) - ((Math.floor(n / CELDAS_DISP) % LADO_CUAD) * CELDAS_DISP)
  for(var i = 0; i < 3; i++){
    for(var j = 0; j < 3; j++){
      result.push(puzzle[cuadro + ((i * 9) + j)]);
    }
  }
  return result;
}

function getCuadroRec(n){
  var result = [];
  var fila = (Math.floor(n / CELDAS_DISP) % LADO_CUAD);
  var columna = (n % CELDAS_DISP % LADO_CUAD);
  var cuadro = n - ((n % CELDAS_DISP) % LADO_CUAD) - (Math.floor(n / CELDAS_DISP) % LADO_CUAD);
  for(var i = 0; i < 3; i++){
    for(var j = 0; j < 3; j++){
      if(i !== fila && j !== columna){
        result.push(puzzle[n + ((i * 9) + j)]);
      }      
    }
  }
  return result;
}9

/*
********************************************************************************
*                              MÉTODOS GENERALES
********************************************************************************
*/


function getPosibilidades (n) {
  var filtered = []
  
  if (puzzle[n] === ' '){
  
    var fila = getFila(n).filter(function(x){return x !== ' '});
    var columna = getColumna(n).filter(function(x){return x !== ' '});
    var cuadro = getCuadro(n).filter(function(x){return x !== ' '}); 
    filtered = ['1', '2', '3', '4', '5', '6', '7', '8', '9'].diff(fila.unique(columna).unique(cuadro));
    
    console.log("Fila: " + fila);
    console.log("Columna: " + columna);
    console.log("Cuadro: " + cuadro);
    console.log("Posibles: " + filtered);
    
  }
  
  return filtered;
  
}

/*
********************************************************************************
*                   MÉTODOS REDUCCIÓN DEL ESPACIO DE ESTADOS
********************************************************************************
*/

function allCandidates(){
  var candidates = []
  puzzle.forEach(function (e, i){
    candidates.push(getPosibilidades(i));
  })
  return candidates;
}

function nakedSingles(){
  var actual;
  for(var i = 0; i < TOTAL_CELDAS; i++){
    actual = posibles[i]
    if(actual.length === 1){
      puzzle[i] = actual[0]
      posibles = allCandidates();
      i = 0
    }
  }
}

function posiblesFila(n){
  var opciones = []
  var fila = Math.floor(n/CELDAS_DISP) * CELDAS_DISP;
  for (var i = 0; i < CELDAS_DISP; i++){
    var dif = getPosibilidades(fila + i);
    opciones = opciones.unique(dif);
  }
  
  return ['1', '2', '3', '4', '5', '6', '7', '8', '9'].diff(opciones);
}

function posiblesColumna(n){
  var opciones = []
  var columna = n % CELDAS_DISP
  for (var i = 0; i < CELDAS_DISP; i++){
    var dif = getPosibilidades(columna + (i * CELDAS_DISP));  
    opciones = opciones.unique(dif);
  }
  return ['1', '2', '3', '4', '5', '6', '7', '8', '9'].diff(opciones);
}

function posiblesCuadro(n){
  var opciones = [1, 2, 3, 4, 5, 6, 7, 8, 9]
  var cuadro = n - ((n % CELDAS_DISP) % LADO_CUAD) - ((Math.floor(n / CELDAS_DISP) % LADO_CUAD) * CELDAS_DISP)
  for(var i = 0; i < 3; i++){
    for(var j = 0; j < 3; j++){
      var dif = getPosibilidades(cuadro + ((i * 9) + j));      
      opciones = opciones.unique(dif);
    }
  }
  return ['1', '2', '3', '4', '5', '6', '7', '8', '9'].diff(opciones);
}

function hiddenSingles(){
  posibles.forEach(function(e, i){
    var candFila = posiblesFila(i);
    var candColumna = posiblesColumna(i);
    var candCuadro = posiblesCuadro(i);
    posibles[i] = ['1', '2', '3', '4', '5', '6', '7', '8', '9'].diff(candFila.unique(candColumna).unique(candCuadro))
  });
}

function reduccion(){
  posibles = allCandidates();  
  hiddenSingles();
  nakedSingles();  
}

/*
********************************************************************************
*                        MÉTODOS FUNCIÓN HEURÍSTICA (A*)
********************************************************************************
*/

function h(n){

  return getPosibilidades(n).length * FACTOR_RIESGO;
  
}

function g(n){

  var fila = getFila(n).filter(function(x){return x === ' '});
  var columna = getColumna(n).filter(function(x){return x === ' '});
  var cuadro = getCuadroRec(n).filter(function(x){return x === ' '}); 

  return fila.length + columna.length + cuadro.length;

}

function reiniciarHeuristicas(){

  heuristicas = puzzle.map(function(x){
    if (x !== ' ') {
      return -1;
    } else {
      return 0;
    }
  }) 
}

function calcularHeuristicas(){
  heuristicas.forEach(function(e, i){
    if(e !== -1){
      heuristicas[i] = h(i) + g(i);
    }
  })
}

function calcularHN(){
  var result = [];
  
  heuristicas.forEach(function(e, i){
    if(e !== -1){
      result[i] = h(i);
    }
  })
  
  return result;
  
}

/*
********************************************************************************
*                   MÉTODOS DE RESOLUCIÓN DEL PUZZLE (A*)
********************************************************************************
*/

function resolverAStar(n){

  var filtered = getPosibilidades (n)
  
  if(error){
    error = false;
    filtered.splice(filtered.indexOf(snaps[snaps.length -1].escogido), 1);
    snaps.splice(snaps.length -1, 1);  
  }
  
  if(filtered.length === 0){
    error = true;
    puzzle = snaps[snaps.length -1].puzzle;
    ultimoVar = snaps[snaps.length -1].resuelto;
    return;
  }
  
  var puzzSnap = [].concat(puzzle);
  
  var data = {puzzle : puzzSnap, valores : filtered, resuelto : n} ;
 
  var resp = [];
  
  filtered.forEach(function(e){
    puzzle[n] = e;
    reiniciarHeuristicas();
    calcularHeuristicas();
    resp.push(heuristicas.reduce(function(prev, cur){
      return prev + cur;
    }))
  })
  
  puzzle[n] = filtered[resp.indexOf(Math.min.apply(Math, resp))];
  
  if(filtered.length > 1){
    data.escogido = puzzle[n];
    snaps.push(data);
  }

}

/*
********************************************************************************
*       MÉTODOS DE RESOLUCIÓN DEL PUZZLE (SATISFACCIÓN DE RESTRICCIONES)
********************************************************************************
*/

function reiniciarRestricciones(){

  restricciones = puzzle.map(function(x){
    if (x !== ' ') {
      return -1;
    } else {
      return 0;
    }
  })
  
}

function calcularRestricciones(){
  restricciones.forEach(function(e, i){
    if(e !== -1){
      var posibilidades = getPosibilidades(i);
      restricciones[i] = posibilidades.length;
    }
  })
  
}

function resolverSatRest(n){

  var filtered = getPosibilidades(n)
  
  if(error){
    error = false;
    filtered.splice(filtered.indexOf(snaps[snaps.length -1].escogido), 1);
    snaps.splice(snaps.length -1, 1);  
  }
  
  if(filtered.length === 0){
    error = true;
    puzzle = snaps[snaps.length -1].puzzle;
    ultimoVar = snaps[snaps.length -1].resuelto;
    return;
  }
  
  var puzzSnap = [].concat(puzzle);  
  var data = {puzzle : puzzSnap, valores : filtered, resuelto : n} ; 
  var resp = [];  
  var restCount;
  
  filtered.forEach(function(e){
    puzzle[n] = e;
    reiniciarRestricciones();
    calcularRestricciones();
    restCount = restricciones.map(function(x){
      return x.count
    });
    
    if(restCount.indexOf(0) !== -1){
      resp.push(INFINITO)
    } else {
      resp.push(parseInt(e))
    }
  })
  
  puzzle[n] = filtered[resp.indexOf(Math.min.apply(Math, resp))];
  
  if(filtered.length > 1){
    data.escogido = puzzle[n];
    snaps.push(data);
  }

}

/*
********************************************************************************
*            MÉTODOS DE RESOLUCIÓN DEL PUZZLE (ALGORITMOS GENÉTICOS)
********************************************************************************
*/

function getFilaGen(puzzle, n){
  var result = [];
  var fila = n%CELDAS_DISP;
  for (var i = 0; i < CELDAS_DISP; i++){
    result.push(puzzle[fila][i]);
  }
  return result;
}

function getColumnaGen(puzzle, n){
  var result = [];
  var columna = n%CELDAS_DISP;
  for (var i = 0; i < CELDAS_DISP; i++){
    result.push(puzzle[i][columna]);
  }
  return result;
}

function getCuadroGen(puzzle, n){
  var result = [];
  var offY = n % 3;
  var offX = Math.floor(n / 3);
  for(var i = 0; i < 3; i++){
    for(var j = 0; j < 3; j++){
      result.push(puzzle[i + 3 * offX][j + 3 * offY]);
    }
  }
  return result;
}

function getPositionOfSetValues(sudoku) {

	var EMPTY = 0;
	
	var indexes = [];
	
	for (var i = 0; i < sudoku.length; i++) {
		for (var j = 0; j < sudoku[i].length; j++) {
			if (sudoku[i][j] != EMPTY) {
				indexes.push((i * 9) + j);
			}
		}
	}
	
	return indexes;
}

function startSudokuRandom() {

	var EMPTY = 0;
	
  var sudoku = [];

	for (var i = 0; i < puzzle.length; i++) {
	  sudoku.push([]);
		for (var j = 0; j < puzzle[i].length; j++) {
			if (puzzle[i][j] == EMPTY) {
				sudoku[i][j] = Math.floor((Math.random() * 9) + 1);
			}else{
  			sudoku[i][j] = puzzle[i][j];
			}
		}
	}
	
	return sudoku;
}

function getFitness(sudoku) {

	var SIZE = 9;
	
	var slice;
	var emptyRepetitions = [];
	var repetitions = [];
	var fitness = 0;
	
	for (var i = 0; i < SIZE; i++) {
		repetitions.push(0);
	}
	
	emptyRepetitions = repetitions.slice(0);
	
	for (var i = 0; i < SIZE; i++) {
		slice = getFilaGen(sudoku, i);
		repetitions = emptyRepetitions.slice(0);
		
		for (var j = 0; j < SIZE; j++) {
			repetitions[parseInt(slice[j]) - 1]++;
			if (repetitions[parseInt(slice[j]) - 1] > 1) {
				fitness++;
			}
		}
	}
	
	for (var i = 0; i < SIZE; i++) {
		slice = getColumnaGen(sudoku, i);
		repetitions = emptyRepetitions.slice(0);
		
		for (var j = 0; j < SIZE; j++) {
			repetitions[parseInt(slice[j]) - 1]++;
			if (repetitions[parseInt(slice[j]) - 1] > 1) {
				fitness++;
			}
		}
	}
	
	for (var i = 0; i < SIZE; i++) {
		slice = getCuadroGen(sudoku, i);
		repetitions = emptyRepetitions.slice(0);
		
		for (var j = 0; j < SIZE; j++) {
			repetitions[parseInt(slice[j]) - 1]++;
			if (repetitions[parseInt(slice[j]) - 1] > 1) {
				fitness++;
			}
		}
	}
	
	return fitness;
}

function sudokuFitted(sudoku) {
	this.sudoku = sudoku;
	this.fitness = getFitness(sudoku);
}

function getFittest(population) {
	var sortedPopulation = [];
	var fittest = [];
	var threshold = 0.1;
	
	for (var i = 0; i < population.length; i++) {
		sortedPopulation.push(new sudokuFitted(population[i]));
	}
	
	sortedPopulation.sort(function (a, b) {return (a.fitness - b.fitness);});
	
	for (var i = 0; i < sortedPopulation.length * threshold; i++) {
		fittest.push(sortedPopulation[i].sudoku);
	}
	
	return fittest;
}

function mutationValueChecking (sudoku, cell) {
	var column = getFilaGen(sudoku, cell % 9);
	var value = column[Math.floor(cell / 9)];
	var candidates = [];
	
	for (var i = 1; i <= column.length; i++) {
		if (column.indexOf(i) == -1) {
			candidates.push(i);
		}
	}
	
	candidates.push(value);
	
	return candidates[0];
}

function getSon(parentA, parentB) {
	var ROW_SIZE = 9;
	var MUT_FACT = 0.9;
	var limit = Math.floor( (Math.random() * (ROW_SIZE - 2))) + 1;
	var mutationValue = Math.random();
	var son = [];
	var mutatedCell = 0;
	
	for (var i = 0; i < limit; i++) {
		var newRow = [];
		for (var j = 0; j < ROW_SIZE; j++) {
			newRow.push(parentA[i][j]);
		}
		son.push(newRow);
	}
	for (var i = limit; i < ROW_SIZE; i++) {
		var newRow = [];
		for (var j = 0; j < ROW_SIZE; j++) {
			newRow.push(parentB[i][j]);
		}
		son.push(newRow);
	}
	
	if (mutationValue < MUT_FACT) {
		for (var i = 0; i < 9; i++) {
			mutatedCell = Math.floor((Math.random() * 81));
			
			if (setValues.indexOf(mutatedCell) == -1) {
				son[Math.floor(mutatedCell / 9)][mutatedCell % 9] = mutationValueChecking(son, mutatedCell);
			}
		}
	}
	
	return son;
}

function adaptSudoku(){
  var newSudoku = []
  for(var i = 0; i < 9; i++){
    newSudoku.push([])
    for(var j = 0; j < 9; j++){
      newSudoku[i][j] = puzzle[(i * CELDAS_DISP) + j]
    }
  }
  
  for(var k = 0; k < 81; k++){
    puzzle.splice(0,1);
  }
 
  newSudoku.forEach(function(e){
    puzzle.push(e)
  })
  
}

function readaptSudoku(fittest){
  console.log(puzzle)
  var newSudoku = []
  for(var i = 0; i < 9; i++){
    for(var j = 0; j < 9; j++){
      newSudoku[(i * CELDAS_DISP) + j] = fittest[i][j]
    }
  }
  console.log(newSudoku)
  
  for(var k = 0; k < 9; k++){
    puzzle.splice(0,1);
  }

  newSudoku.forEach(function(e){
    puzzle.push(e)
  })
  
}

/*
********************************************************************************
*                      MÉTODOS DE EJECUCIÓN DE ALGORITMOS
********************************************************************************
*/

function aStar(puzzle){
     
    reiniciarHeuristicas();
    
    if(heuristicas.filter(function(x){return x !== -1}).length === 0){
    
      return;
    
    } else {     
    
      calcularHeuristicas();
      
      if(error){
        resolverAStar(ultimoVar);
      } else {   
      
        var cuadroElegido = heuristicas.indexOf(heuristicas.minNat());
        
        resolverAStar(cuadroElegido);

      }
      
      return aStar(puzzle);
    
    }
    
}

function satRest(puzzle){

  reiniciarRestricciones();
  calcularRestricciones();
  
  if(restricciones.filter(function(x){return x !== -1}).length === 0){    

    return;

  } else {
  
      if(error){
      
        resolverSatRest(ultimoVar);
        
      } else {
        
        var cuadroElegido = restricciones.indexOf(restricciones.minNat());
        
        resolverSatRest(cuadroElegido);

      }
      
      return satRest(puzzle)
      
  }
  
  return puzzle;
  
}

function geneticos(puzzle){
  console.log("Original sudoku: " + puzzle)
  adaptSudoku();
  console.log(puzzle)
  setValues = getPositionOfSetValues(puzzle);
 	var parents = [];
	var generation = 0;
	var GEN_SIZE = 5000;
	var NUM_GENERATIONS = 200;
	for (var i = 0; i < GEN_SIZE; i++) {
		var sudoku = startSudokuRandom();
		parents.push(sudoku);
	}
	
	var sortedPopulation = getFittest(parents);
	
	while (generation <= NUM_GENERATIONS && getFitness(sortedPopulation[0]) > 0) {
		var newGeneration = [];
		
		for (var i = 0; i < GEN_SIZE/2; i++) {
			var parentA = sortedPopulation[Math.floor(Math.random() * sortedPopulation.length)];
			var parentB = sortedPopulation[Math.floor(Math.random() * sortedPopulation.length)];
			newGeneration.push(getSon(parentA, parentB));
		}
		
		sortedPopulation = getFittest(newGeneration);

		console.log("Best fit: " + sortedPopulation[0]);

		console.log("Fitness: " + getFitness(sortedPopulation[0]));

		console.log("Gen num: " + generation);

		console.log("Fittest group Size: " + sortedPopulation.length);
		
		generation++;
	}
	
	console.log("Best fit: " + sortedPopulation[0]);

  readaptSudoku(sortedPopulation[0]);

	console.log("Fitness: " + getFitness(sortedPopulation[0]));

	console.log("Ending");
	
	return getFitness(sortedPopulation[0]);
}

/*
********************************************************************************
*                            RECURSOS WEB SERVER
********************************************************************************
*/

exports.index = function(req, res){
  res.render('index', { title: 'Sudoku' });
};

exports.resolver = function(req, res){
  console.log(util.inspect(req.body, false, null))
  heuristicas = [];
  error = false;
  snaps = [];  
  restricciones = []
  geneticPuzzle = [];
  
  var original = req.body.puzzle;
  puzzle = req.body.puzzle;
  var algoritmo = parseInt(req.body.algoritmo);
  var nombreAlg;
  if(req.body.reduccion === '1'){
    reduccion()
  }
  var hd = new memwatch.HeapDiff();
  var start = process.hrtime()
  var comm;
  
  console.log("Recibiendo puzzle")
  console.log("Puzzle: " + puzzle)
  
  switch(algoritmo){
    case 1: aStar(puzzle);
            nombreAlg = "A*"
            break;
    case 2: satRest(puzzle, 0);
            nombreAlg = "Forward Checking"
            break;    
    case 3: comm = geneticos(puzzle);
            nombreAlg = "Algoritmos Genéticos"
            break;
  }

  console.log("Resuelto: " + puzzle)
  var diff = process.hrtime(start)
  var memory = hd.end();
  var time = (diff[0] * 1e9 + diff[1]) / 1e9;
  var data = {puzzle: puzzle, time : time, memory : memory.change.size, comments: comm};
  var logData = "Puzzle: " + original + " | Solución: " + puzzle + " | Tiempo: " + time + "s | Memoria: " + memory.change.size + " | Algoritmo: " + nombreAlg + " | Fecha(UTC): " + new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') + "\n"
  fs.open('public/javascripts/log.txt', 'a', 666, function( e, id ) {
    console.log(id)
    console.log(e)
    fs.write(id, logData, null, 'utf8', function(){
      fs.close(id, function(){
        console.log('Logs guardados');
      });
    });
  });
  res.json({data: data});
}
