'use strict'

var dificultad;
var puzzle;

function llenarSudoku(){
  for(var i = 0; i < 9; i++){
    for(var j = 0; j < 9; j++){
      var index = "data" + i + "" + j;
      $("#"+index).html(puzzle[(i*9) + j]);
    }
  }  
}

function getSudoku(){
  $.getJSON("/javascripts/sudoku.json", function(data) {
    puzzle = data[dificultad][Math.floor((Math.random()*10)+1)].replace(/\./g, " ").split("");    
    llenarSudoku();
  });
}

function error(){
  $(".square").addClass("invisible")
  $("#sudoku").html("<center>Hubo un error con el servidor :( <br /><br /><blockquote>\"Hay una puerta por la que pueden entrar la buena o la mala fortuna, pero tú tienes la llave.\"<br /><br /> <t />Proverbio japonés</blockquote></center>")
}

function borrarSudoku(){
  for(var i = 0; i < 9; i++){
    for(var j = 0; j < 9; j++){
      var index = "data" + i + "" + j;
      $("#"+index).html("");
    }
  }  
}

$(function(){
  
  $("input[name=dificultad]").click(function(){
  
    var formDificultad = $(this).next('label').text().replace(/\s+/g, '');
  
    if(dificultad === undefined || formDificultad !== dificultad){
  
      dificultad = formDificultad
      
      $('#Cambiar').prop('disabled', false);
      
      getSudoku();
      
    }
    
  })
  
  $('#Cambiar').click(function(){
    getSudoku();
  })
  
  $("#Resolver").click(function(){
  
    if($('input[name=dificultad]').is(':checked') && $('input[name=algoritmo]').is(':checked')) {
  
      var algoritmo = $('input[name=algoritmo]:checked').val()
      var reduccion = $('input[name=reduccion]').is(':checked') ? 1 : 0;
      var data = {puzzle : puzzle, algoritmo : algoritmo, reduccion: reduccion}
      
      console.log(data)
      
      $('#Resolver').addClass('invisible')
      $('input').prop('disabled', true)
      $('body').addClass('busy')
        
      $.ajax({
        url: "/Resolver",
        type: "post",
        error: error,
        data: data,
        success: function(result){
          console.log(result)
          
          $('#Borrar').prop('disabled', false)
          $('#Borrar').removeClass('invisible')       
                 
          puzzle = result.data.puzzle        
          console.log(puzzle)        
          llenarSudoku();
          
          if(result.data.comments){
            $('#comm').removeClass('invisible')
            $('#comm').html("ULTIMO FITNESS: " + result.data.comments);
          }
                  
          $("#Tiempo").html(result.data.time + " s")        
          $("#Memoria").html(result.data.memory)        
          $('body').removeClass('busy')
          
        }
      })
    }
  })
  
  $('#Borrar').click(function(){
    $('#comm').html('');  
    $('#comm').addClass('invisible')
    $('.Warning').addClass('invisible')    
    $('input').prop('disabled', false)
    $('#Cambiar').prop('disabled', true)
    
    //Desabilitar reduccion (hasta que esté disponible)
//    $('#Reduccion').prop('disabled', true)
    
    $('#Borrar').addClass('invisible')
    $('#Resolver').removeClass('invisible')
    
    dificultad = undefined;
    puzzle = undefined;
    borrarSudoku()
    
  })
  
  $('#geneticos').click(function(){
    $('#msg').removeClass('invisible')
  })
  
  $('#satrest').click(function(){
    $('#msg').addClass('invisible')
  })
  
  $('#astar').click(function(){
    $('#msg').addClass('invisible')
  })  
  
})
